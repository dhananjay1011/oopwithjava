package BASIC_FUNCTIONALITY_CLASS;

public class Invoice {
	private String pnum;
	private String pdesc;
	private int qty;
	private double price;
	
	public Invoice(String pnum, String pdesc, int qty, double price) {
		super();
		this.pnum = pnum;
		this.pdesc = pdesc;
		if(qty<0)
			this.qty=0;
		else
		this.qty = qty;
		if(price<0)
			this.price=0;
		else
		this.price = price;
	}
	
    public String getPnum() {
		return pnum;
	}

	public void setPnum(String pnum) {
		this.pnum = pnum;
	}

	public String getPdesc() {
		return pdesc;
	}

	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		if(qty<0)
			this.qty=0;
		else
		this.qty = qty;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if(price<0)
			this.price=0;
		else
		this.price = price;
	}

	public double getInvoiceAmount() {
		double amt=this.price*this.qty;
		return amt;
	}
}