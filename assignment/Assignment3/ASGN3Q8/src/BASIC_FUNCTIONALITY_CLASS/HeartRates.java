//Create a class called HeartRates. The class attributes should
//include the person�s first name, last name and date of birth
//(consisting of separate attributes for the month, day and year
//of birth). Your class should have a constructor that receives
//this data as parameters. For each attribute provide set and get
//methods. The class also should include a method that calculates
//and returns the person�s age (in years), a method that
//calculates and returns the person�s maximum heart rate and a
//method that calculates and returns the person�s target
//heart rate. Write a Java application that prompts for the
//person�s information, instantiates an object of class HeartRates
//and prints the information from that object�including the
//person�s first name, last name and date of birth�then calculates
//and prints the person�s age in (years), maximum heart
//rate and target-heart-rate range.
package BASIC_FUNCTIONALITY_CLASS;

public class HeartRates {
	
	private String fname,lname;
	public  Date birth=new Date(1, 1, 2020);
	public HeartRates() {
	
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	
	public void age() {
		System.out.println("Age: "+(2020-getBirth().getYear()));
		
	}
	public void maximum_heart_rate() {
		System.out.println("maximum_heart_rate: "+(220-(2020-getBirth().getYear())));
	}
	public void target_heart_rate() {
		System.out.println("target_heart_rate: "+(0.5*(220-(2020-getBirth().getYear())))+" to "+(0.85*(220-(2020-getBirth().getYear()))));
		
	}
	public Date getBirth() {
		return birth;
	}
	public void setBirth(Date birth) {
		this.birth = birth;
	}

}

