package BASIC_FUNCTIONALITY_CLASS;

public class BodyMassIndex {
	private double weight,height, bmi;
	private String status;
	public double getBmi() {
		bmi=(weight/(height*height)*703);
		if(bmi< 18.5)
			setStatus("Underweight");
		else if (bmi>=18.5 && bmi<=24.9) 
			setStatus("Normal");
		else if (bmi>24.9 && bmi<=29.9) 
			setStatus("Overweight");
		else if (bmi>=30)
			setStatus("Obese");
		return bmi;
	}
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BodyMassIndex(double weight, double height) {
		super();
		this.weight = weight;
		this.height = height;
	}
	
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	{
	
}
}
