package BASIC_FUNCTIONALITY_CLASS;
//The class attributes should include the person�s first name,
//last name, gender, date of birth (consisting of separate
//attributes for the month, day and year of birth), height (in
//inches) and weight (in pounds).
//Your class should have a constructor that receives this data.
//For each attribute, provide set and get methods.
//The class also should include methods that calculate and return
//the user�s age in years, maximum heart rate and target-heartrate range, and body mass index.
//Write a Java application that prompts for the person�s
//information, instantiates an object of class HealthProfile for
//that person and prints the information from that object�
//including the person�s first name, last name, gender, date of
//birth, height and weight�then calculates and prints the person�s
//age in years, BMI, maximum heart rate and target-heart-rate
//range.
//Note : Use previous assignment for calculating BMI and heart
//rate

public class HealthProfile{
		private String gender;
		private BodyMassIndex bmi=new BodyMassIndex(0, 0);
		private HeartRates hr= new HeartRates();
		
		public HealthProfile(String gender) {
			super();
			this.gender = gender;
			
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public void AcceptRecord() {
			System.out.print("Enter first name: ");
			hr.setFname(Date.sc.nextLine());
			System.out.print("Enter last name: ");
			hr.setLname(Date.sc.nextLine());
			System.out.print("Enter birth year: ");
			hr.birth.setYear(Date.sc.nextInt());
			System.out.print("Enter birth month: ");
			hr.birth.setMonth(Date.sc.nextInt());
			System.out.print("Enter birth day: ");
			hr.birth.setDay(Date.sc.nextInt());
			Date.sc.nextLine();
			System.out.print("Enter gender:");
			setGender(Date.sc.nextLine());
			System.out.print("Enter weight in pounds:");
			bmi.setWeight(Date.sc.nextDouble());
			System.out.print("Enter height in inches:");
			bmi.setHeight(Date.sc.nextDouble());
		}
		public void printRecord() {
			System.out.println(" first name: "+hr.getFname());
			System.out.println(" last name: "+hr.getLname());
			System.out.println("Birthdate: "+hr.birth.getDay()+"/"+hr.birth.getMonth()+"/"+hr.birth.getYear());
			hr.age();
			hr.maximum_heart_rate();
			hr.target_heart_rate();
			System.out.println("BMI: "+String.format("%.2f",bmi.getBmi()));
			System.out.println(bmi.getStatus());
		}
		
		

	

	
}
	