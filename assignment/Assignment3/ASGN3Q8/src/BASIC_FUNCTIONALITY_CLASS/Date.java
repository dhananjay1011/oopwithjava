package BASIC_FUNCTIONALITY_CLASS;

import java.util.Scanner;

public class Date {
	static Scanner sc= new Scanner(System.in);
	private	int month;
	private int day;
	private int year;
	public Date(int day , int month , int year){
	
		if(month>0 && month<13) {
			if((year%4==0 && year%100!=0 || year%400==0) && month==2 && day<30) {
				this.setDay(day);
				this.setMonth(month);
				this.setYear(year);}
				
			else if((month%2==0 || month!=2 ||month==8) && day<32) {
				this.setDay(day);
				this.setMonth(month);
				this.setYear(year);}
			}
		else
		System.out.println("Invalid date");
	}
	public void setMonth(int month) {
		if(month>0 && month<13)
			this.month = month;
		else 
			this.month = 1;
			
	}
	public void setDay(int day) {
		if((getYear()%4==0 && getYear()%100!=0 || getYear()%400==0) && getMonth()==2 && day<30)
		this.day = day;
		else if((getMonth()%2==0 || getMonth()!=2 ||getMonth()==8) && day<32) 
			this.day=day;
		else
			this.day=1;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getYear() {
		return year;
	}
	public int getMonth() {
		return month;
	}
	public int getDay() {
		return day;
	}
}
