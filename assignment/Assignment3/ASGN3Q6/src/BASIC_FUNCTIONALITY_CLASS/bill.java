package BASIC_FUNCTIONALITY_CLASS;

import java.util.Scanner;

public class bill{
	  Scanner user_input = new Scanner(System.in);
	  String plan;
	  float call_charges;
	  float sms_charges;
	  float monthly_charges;
	  float Bill_Charges=0;
	  static final float Service_Tax=1.125f;
	  int number_of_calls;
	  int number_of_sms;
	  int first_calls;
	  int next_calls;
	  float total_charges;
	  float Final_Bill;
	  
	  
	  public String getplan() {
		return plan;
	}
	  public void setCall_charges(float d) {
		this.call_charges = d;
	}
	  public void setBill_Charges(float bill_Charges) {
		Bill_Charges = bill_Charges;
	}
	  public void setMonthly_charges(float monthly_charges) {
		this.monthly_charges = monthly_charges;
	}
	  public void setSms_charges(float sms_charges) {
		this.sms_charges = sms_charges;
	}
	public void setNext_calls(int next_calls) {
		this.next_calls = next_calls;
	}
	public void setFirst_calls(int first_calls) {
		this.first_calls = first_calls;
	}
	
	public void setplan(String plan) {
		this.plan = plan;
		if(plan.equalsIgnoreCase("a")) {
			setCall_charges(1.00f);
			setSms_charges(1.00f);
			setMonthly_charges(199.0f);
			setFirst_calls(50);
			setNext_calls(50);
		}
		else if(plan.equalsIgnoreCase("b")) {
			setCall_charges(0.80f);
			setSms_charges(0.75f);
			setMonthly_charges(299.0f);
			setFirst_calls(75);
			setNext_calls(75);
		}
		else if(plan.equalsIgnoreCase("c")) {
			setCall_charges(0.60f);
			setSms_charges(0.5f);
			setMonthly_charges(399.0f);
			setFirst_calls(100);
			setNext_calls(100);
		}
		else
			System.out.println("Invalid choice");
			
	}
	public int getNumber_of_calls() {
		return number_of_calls;
	}
	public void setNumber_of_calls(int number_of_calls) {
		this.number_of_calls = number_of_calls;
	}
	public int getNumber_of_sms() {
		return number_of_sms;
	}
	public void setNumber_of_sms(int number_of_sms) {
		this.number_of_sms = number_of_sms;
	}
	public void accept(){
		System.out.println("Enter your plan: ");
		setplan(user_input.nextLine());
		System.out.println("Enter Number of calls made: ");
		setNumber_of_calls(user_input.nextInt());
		System.out.println("Enter Number of SMS sent: ");
		setNumber_of_sms(user_input.nextInt());
	  }
	public void display() {
		System.out.println(getplan());
		System.out.println(getNumber_of_sms());
		System.out.println(getNumber_of_calls());
		System.out.println(Final_Bill);
		System.out.println(monthly_charges);
		System.out.println(Bill_Charges);

	}
	public void charges() {
		if(number_of_calls>first_calls) {
			if(number_of_calls-first_calls<next_calls) {
				Bill_Charges=(float) ((number_of_calls-first_calls)*call_charges*0.5);}
			if(number_of_calls-first_calls-next_calls>0) {
				Bill_Charges=(float) ((float) (next_calls)*call_charges*0.5);
				Bill_Charges=Bill_Charges+ (number_of_calls-first_calls-next_calls)*call_charges;}
			Bill_Charges=Bill_Charges+number_of_sms*sms_charges;
			total_charges=Bill_Charges+monthly_charges;
		Final_Bill=total_charges*Service_Tax;
		}
	}
			
}
	