package TEST_CLASS;

import java.util.Scanner;

import BASIC_FUNCTIONALITY_CLASS.Employee;

public class EmployeeTest {
	 Employee i1=new Employee(null, null, 0) ;
	
	Scanner sc= new Scanner(System.in);
	public void AcceptRecord(int i) {
		System.out.print("Enter "+i+" first name: ");
		i1.setfname(sc.nextLine());
		System.out.print("Enter "+(i)+" last name: ");
		i1.setlname(sc.nextLine());
		System.out.print("Enter "+(i)+" salary: ");
		i1.setsal(sc.nextDouble());		
	}
	public void DisplayRecord(int i) {
		System.out.println("\n"+i+".First name: "+i1.getfname());
		System.out.println(i+".Last name: "+i1.getlname());
		System.out.println(i+".Salary: "+String.format("%.2f",i1.getsal()));
		System.out.println(i+".Yearly salary: "+String.format("%.2f",i1.getYearlySal()));
	}
	public void raise(int i) {
		System.out.println("\n"+i+".After raise: "+String.format("%.2f",i1.raise()));
	}
	


		
}


