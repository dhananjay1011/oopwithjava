package BASIC_FUNCTIONALITY_CLASS;
//Q.2 Create a class called Employee that includes three instance
//variables�a first name (type String), a last name (type String)
//and a monthly salary (double). Provide a constructor that
//initializes the three instance variables. Provide a set and a
//get method for each instance variable. If the monthly salary is
//not positive, do not set its value. Write a test application
//named EmployeeTest that demonstrates class Employee�s
//capabilities. Create two Employee objects and display each
//object�s yearly salary. Then give each Employee a 10% raise and
//display each Employee�s yearly salary again.
public class Employee {
	private  String fname;
	private  String lname;
	private double sal;

	public Employee(String fname, String lname, double sal) {
		this.fname=fname;
		this.lname=lname;
		if (sal<0)
			this.sal=0.0;
		else
		this.sal=sal;}
	
	
	public  String getfname() {
		return fname;}

	public  String getlname() {
		return lname;}

	public double getsal() {
		return sal;}

	public void setfname(String fname) {
		this.fname=fname;}

	public void setlname(String lname) {
		this.lname=lname;}

	public void setsal(double sal) {
		if (sal<0)
			this.sal=0.0;
		else
			this.sal=sal;}

	public double getYearlySal() {
		return this.sal*12;
	}

	public double raise() {
		return this.getYearlySal()*1.10;
	}
	
}