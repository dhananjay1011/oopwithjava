package BASIC_FUNCTIONALITY_CLASS;
//Q.3 Create a class called Date that includes three instance
//variables�a month (type int), a day (type int) and a year (type
//int). Provide a constructor that initializes the three instance
//variables and assumes that the values provided are correct.
//Provide a set and a get method for each instance
//variable. Provide a method displayDate that displays the month,
//day and year separated by forward
//slashes (/).
//Write a test application named DateTest that demonstrates class
//Date�s capabilities.
public class Date {
	public	int month;
	public int day;
	public 	int year;
	public Date(int day , int month , int year){
	
		if(month>0 && month<13) {
			if((year%4==0 && year%100!=0 || year%400==0) && month==2 && day<30) {
				this.day=day;
				this.month=month;
				this.year=year;}
				
			else if((month%2==0 || month!=2 ||month==8) && day<32) {
				this.day=day;
				this.month=month;
				this.year=year;}
			}
		else
		System.out.println("Invalid date");
	}
	public void setMonth(int month) {
		if(month>0 && month<13)
			this.month = month;
		else 
			this.month = 1;
			
	}
	public void setDay(int day) {
		if((year%4==0 && year%100!=0 || year%400==0) && month==2 && day<30)
		this.day = day;
		else if((month%2==0 || month!=2 ||month==8) && day<32) 
			this.day=day;
		else
			this.day=1;
	}
	public void setYear(int year) {
		this.year = year;
	}
}
