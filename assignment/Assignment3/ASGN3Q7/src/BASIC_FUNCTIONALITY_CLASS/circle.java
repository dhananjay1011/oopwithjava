package BASIC_FUNCTIONALITY_CLASS;

import java.util.Scanner;

public class circle{
	  Scanner user_input = new Scanner(System.in);
	  int radius;
	  double diameter;
	  double circumference;
	  double area;
	  
	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public double getDiameter() {
		return diameter;
	}

	public void setDiameter(float diameter) {
		this.diameter = diameter;
	}

	public double getCircumference() {
		return circumference;
	}

	public void setCircumference(float circumference) {
		this.circumference = circumference;
	}

	public double getArea() {
		return area;
	}

	public void setArea(float area) {
		this.area = area;
	}

	public void accept() {
		System.out.println("Enetr radius: ");
		setRadius(user_input.nextInt());
		changes();
	}

	void changes() {
		area=Math.PI*Math.pow( radius, 2);
		circumference=(2*Math.PI*radius);
		diameter=radius*2;
		
	}

	public void display() {
		System.out.println("Radius: "+ radius);
		System.out.println("Dia: "+String.format("%.2f", diameter));
		System.out.println("circumference: "+String.format("%.2f", circumference));
		System.out.println("Area: "+String.format("%.2f", area));
		
	}
	 
			
}
	