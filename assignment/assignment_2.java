import java.util.Scanner;
class Program{

    //For Prime Number
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int n, Num = 2;
        for(Num = 2; Num < number; Num++)
        {
            if(number%Num == 0){
                System.out.println(number+" is not Prime Number");
                break;
            }

        }
        if(number == Num)
            System.out.println(number+" is Prime Number");
    }

    //For Armstrong Number
    public static void main5(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int remainder, sum = 0, Original_Num;
        Original_Num = number;
        while(number != 0)
        {
            remainder = number%10;
            sum = sum + (int)Math.pow((double)remainder, 3);
            number = number / 10;
        }
        if(Original_Num == sum)
            System.out.println(Original_Num+" is Armstrong Number");
        else
            System.out.println(Original_Num+" is not Armstrong Number");
    }

    //For Strong Number
    public static void main4(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int remainder, n, sum = 0, Original_Num, i;
        Original_Num = number;
        while(number != 0)
        {
            remainder = number%10;
            for(i = 1, n = 1; i <= remainder; i++)
            {
                n = n * i;
            }
            number = number / 10;
            sum = sum + n;
        }
        if(Original_Num == sum)
            System.out.println(Original_Num+" is Strong Number");
        else
            System.out.println(Original_Num+" is not Strong Number");
    }

    //For Perfect Number
    public static void main3(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int num, n = 0;
        for(num = 1; num < number; num++)
        {
            if(number%num == 0)
            {
                n = n + num;
            }
        }
        if(number == n)
            System.out.println(number+" is Perfect Number");
        else
            System.out.println(number+" is not Perfect Number");
    }

    //For Reverse Number and Palindrome Number
    public static void main2(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int temp, remainder = 0, Original_Num;
        Original_Num = number;
        while(number != 0 )
        {
            temp = number%10;
            remainder = remainder * 10 + temp;
            number = number/10;
        }
        System.out.println("Reverse Number of "+Original_Num+" : "+remainder);

        if(Original_Num == remainder)
            System.out.println("Number is also Palindrome");
        else
            System.out.println("Number is Not Palindrome");
    }

    // Sum of Digits of given number
    public static void main1(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int temp, sum = 0;
        while(number != 0 )
        {
            temp = number%10;
            number = number/10;
            sum = sum + temp;
        }
        System.out.println("Sum : "+sum);
    }
}
-------------------------------------------------------------------------------------------

/*  4. Write a program for addition, subtraction, multiplication and division of two numbers.   */

import java.util.Scanner;
class Program{
    //division
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number1 : ");
        int number1 = sc.nextInt();
        System.out.println("Enter Number2 : ");
        int number2 = sc.nextInt();
        int result;
        if(number2 == 0)
            System.out.println("Divide By Zero ");
        else{
        result = number1 / number2;
        System.out.println("division of two number : "+result);
        }
    }

    //multiplication
    public static void main3(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number1 : ");
        int number1 = sc.nextInt();
        System.out.println("Enter Number2 : ");
        int number2 = sc.nextInt();
        int result;
        result = number1 * number2;
        System.out.println("multiplication of two number : "+result);
    }

    //subtraction
    public static void main2(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number1 : ");
        int number1 = sc.nextInt();
        System.out.println("Enter Number2 : ");
        int number2 = sc.nextInt();
        int result;
        result = number1 - number2;
        System.out.println("subtraction of two number : "+result);
    }

    //Addition
    public static void main1(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number1 : ");
        int number1 = sc.nextInt();
        System.out.println("Enter Number2 : ");
        int number2 = sc.nextInt();
        int result;
        result = number1 + number2;
        System.out.println("Addition of two number : "+result);
    }
}
---------------------------------------------------------------------------------------
/*  5. Modify above assignment and write a menu driven program. Accept input from command prompt.   */

import java.util.Scanner;
class Program{
    public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int choice, number1, number2, result;
    
    do{
        System.out.println("0. Exit");
        System.out.println("1. Addition");
        System.out.println("2. Subtraction");
        System.out.println("3. Multiplication");
        System.out.println("4. Division");

        System.out.println("Enter Choice : ");
        choice = sc.nextInt();

        switch(choice)
        {
            case 1://Addition
                System.out.println("Enter Number1 : ");
                number1 = sc.nextInt();
                System.out.println("Enter Number2 : ");
                number2 = sc.nextInt();
                result = number1 + number2;
                System.out.println("Addition of two number : "+result);
            break;
            case 2://Subtraction
                System.out.println("Enter Number1 : ");
                number1 = sc.nextInt();
                System.out.println("Enter Number2 : ");
                number2 = sc.nextInt();
                result = number1 - number2;
                System.out.println("subtraction of two number : "+result);
            break;
            case 3://Multiplication
                System.out.println("Enter Number1 : ");
                number1 = sc.nextInt();
                System.out.println("Enter Number2 : ");
                number2 = sc.nextInt();
                result = number1 * number2;
                System.out.println("multiplication of two number : "+result);
            break;
            case 4:// Division
                System.out.println("Enter Number1 : ");
                number1 = sc.nextInt();
                System.out.println("Enter Number2 : ");
                number2 = sc.nextInt();
                if(number2 == 0)
                    System.out.println("Divide By Zero ");
                else{
                    result = number1 / number2;
                    System.out.println("division of two number : "+result);
                }
            break;
        }
    }while(choice != 0);
    }
}
-------------------------------------------------------------------------------------------------------
/*  6. Create a BMI (Body Mass Index) calculator that reads the user’s weight in pounds and height in inches (or, if you prefer,
the user’s weight in kilograms and height in meters), then calculates and displays the user’s body mass index.
The formula for calculating BMI is
 BMI = (WeightInKiloGrams) / (HeightInMeters * HeightInMeters);
BMI VALUES
Underweight if BMI is less than 18.5 
Normal if BMI is in between 18.5 and 24.9 
Overweight if BMI is in between 25 and 29.9 
Obese if BMI is 30 or greater    */

import java.util.Scanner;

class Program{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Weight(in kgs) : ");
        float w = sc.nextFloat();
        System.out.print("Enter Height(in m) : ");
        float h = sc.nextFloat();
        float BMI;
        BMI = w / ( h * h );
        System.out.printf("Your Body Mass Index : %.1f\n", BMI);
        if(BMI < 18.5 )
            System.out.println("Your weight is Underweight");
        else{
            if((BMI >= 18.5) && (BMI <= 24.9)) 
                System.out.println("Your weight is Normal");
            else{ 
                if((BMI >= 25) && (BMI <= 29.9))
                    System.out.println("Your weight is Overweight");
                else
                    System.out.println("Your weight is Obese");
            }
        }
    }
}
----------------------------------------------------------------------------------------------------------------
/*  7. Create an application that calculates your daily driving cost, so that you can estimate how much money could be saved by car pooling, which also has other advantages such as reducing carbon emissions and reducing traffic congestion. The application should input the following information and display the user’s cost per day of driving to work:
a) Total miles driven per day.
b) Cost per gallon of gasoline.
c) Average miles per gallon.
d) Parking fees per day.
e) Tolls per day.   */

import java.util.Scanner;
class Program{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Total miles driven per day : ");
        float total_miles = sc.nextFloat();
        System.out.print("Enter Cost per gallon of gasoline : ");
        float cost_per_gallon = sc.nextFloat();
        System.out.print("Enter Average miles per gallon : ");
        float avg = sc.nextFloat();
        System.out.print("Enter Parking fees per day : ");
        float parking_fee = sc.nextFloat();
        System.out.print("Enter Tolls per day : ");
        float toll = sc.nextFloat();

        float total_cost;
        total_cost = parking_fee + toll + ( cost_per_gallon * total_miles ) / avg;

        System.out.print("Your daily driving cost : "+total_cost);  
    }
}


/*
1 gallon = 3.78541 litre

a) Total miles driven per day.
b) Cost per gallon of gasoline. =>100
c) Average miles per gallon. =>70
d) Parking fees per day.
e) Tolls per day.

70                      100
Total miles per day     ?

? = (100 * Total miles per day) / 70


Total cost = Parking fees + Tolls + ?

*/
--------------------------------------------------------------------------------------------------------------
/*  8. Input the current world population and the annual world population growth rate. Write an application to display the estimated world population after one, two, three, four and five years.   */

import java.util.Scanner;

class Program{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter current world population : ");
        long cur_pop = sc.nextLong();
        System.out.print("Enter annual world population growth rate : ");
        float rate = sc.nextFloat();

        int n = 1;
        long population;

        for(n = 1; n <= 5; n++)
        {
            population = Math.round((double)cur_pop * Math.pow( (1 + (rate/100)), n));
            System.out.println("Estimated world population after "+n+" years : "+population);
        }
    }
}

/* 
P = population at now
R = rate per annum

Population after n years = P ((1 + (R/100))^n)

Population n years ago = P /((1 + (R/100))^n)
*/
-------------------------------------------------------------------------------------------------------------------
/*  9. Write a program to accept 3 digits and print all possible combination of these three digits
(For example if three digits are 1, 2 and 3 then all possible combinations are 123,132,231,213,321 and 312) */

import java.util.Scanner;
class Program{
    public static void main(String[] args){
        
        int[] input = null;
        input = new int[3];
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter first digit : ");
        input[0] = sc.nextInt();
        System.out.println("Enter second digit : ");
        input[1] = sc.nextInt();
        System.out.println("Enter third digit : ");
        input[2] = sc.nextInt();
        
        for (int x = 0; x < 3; x++) 
        {
            for (int y = 0; y < 3; y++) 
            {
                for (int z = 0; z < 3; z++) 
                {
                    if (x != y && y != z && z != x){
                        System.out.println(input[x] + "" + input[y] + "" + input[z]);
                    }
                }
            }
        }
    }
}----------------------------------------------------------------------------------------------------------------
/* 1. Write a java program to print simple "Hello World!!!" */ 
class Program{
    public static void main(String[] args) {
        System.out.println("Hello World!!!");
    }
}
----------------------------------------------------------------------------------------------------------------------
/* 2. Write a java program to check type compatibility for following statements. Observe the effect. Make changes in terms of casting if needed and also display the width of all the above data types.
* int : 9348.39
* long int : 100
* short : 80999
* long : 2373467e18
* byte : 129
* float : 218.928
* double : 2930.3f
* char : -3
* boolean : 0   */

class Program{
    public static void main(String[] args) {
        int num1 = (int)9348.39;
        System.out.println("Double to Integer : "+num1);
        long num2 = (long)100;
        System.out.println("Integer to Long : "+num2);
        short num3 = (short)80999;
        System.out.println("Integer to short : "+num3);
        long num4 = (long)2373467e18;
        System.out.println("Integer to Long : "+num4);
        byte num5 = (byte)129;
        System.out.println("Integer to Byte : "+num5);
        float num6 = (float)218.928;
        System.out.println("Double to Float : "+num6);
        double num7 = (double)2930.3f;
        System.out.println("Float to Double : "+num7);
        char num8 = (char)-3;
        System.out.println("Unsigned Integer to Char : "+num8);
        //boolean num9 = (boolean)0;    //Error int cannot convert into boolean
        //System.out.println("Integer to Long Integer : "+num9);
       System.out.println("Size of Integer(in Bits) : "+Integer.SIZE);
       System.out.println("Size of Long(in Bits) : "+Long.SIZE);
       System.out.println("Size of Short(in Bits) : "+Short.SIZE);
       System.out.println("Size of Double(in Bits) : "+Double.SIZE);
       System.out.println("Size of Byte(in Bits) : "+Byte.SIZE);
       //System.out.println("Size of Boolean : "+Boolean.SIZE); //error
       System.out.println("Size of Float(in Bits) : "+Float.SIZE);
       System.out.println("Size of Char(in Bits) : "+Character.SIZE);
       
    }
}
