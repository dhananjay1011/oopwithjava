/**
 * 
 */
package org.sunbeam.dac.model;

/**
 * @author Admin
 *
 */
public class BasePlus_Commission_Employee extends Commission_Employee
{
	double baseSalary;
	
	public BasePlus_Commission_Employee() {
		// TODO Auto-generated constructor stub
	}
	
	
	public BasePlus_Commission_Employee(double baseSalary) {
		super();
		this.baseSalary = baseSalary;
	}


	public double getBaseSalary() {
		return baseSalary;
	}


	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}


	@Override
	public String toString() {
		return super.toString()+"BasePlus_Commission_Employee [baseSalary=" + baseSalary + "]";
	}


	@Override
	public String showSalary() {
		// TODO Auto-generated method stub
		
		return "BasePlus_Commission_Employee [baseSalary=" + baseSalary + ", salary="+getSalary()+getBaseSalary()
	    +"]";
	}

}
