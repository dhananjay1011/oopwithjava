/**
 * 
 */
package org.sunbeam.dac.model;

/**
 * @author Admin
 *
 */
public abstract class Employee {

	String firstName;
	String lastName;
	int ssn;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	
	

	public Employee(String firstName, String lastName, int ssn) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
	}



	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public int getSsn() {
		return ssn;
	}



	public void setSsn(int ssn) {
		this.ssn = ssn;
	}


	public abstract String showSalary();

	@Override
	public String toString() {
		return "Employee [firstName=" + firstName + ", lastName=" + lastName + ", ssn=" + ssn + "]";
	}

	



	
	
	
	
}
