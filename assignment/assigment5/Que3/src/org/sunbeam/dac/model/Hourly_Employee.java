/**
 * 
 */
package org.sunbeam.dac.model;

/**
 * @author Admin
 *
 */
public class Hourly_Employee extends Employee {

	int wage;
	int hours;
	
	public Hourly_Employee() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public int getwage() {
		return wage;
	}



	public void setwage(int wage) {
		this.wage = wage;
	}



	public int gethours() {
		return hours;
	}



	public void sethours(int hours) {
		this.hours = hours;
	}


	

	@Override
	public String toString() {
		
		return super.toString()+"\nHourly_Employee [wage=" + wage + ", hours=" + hours + "]";
	}



	@Override
	public String showSalary() {
		// TODO Auto-generated method stub
		double salary=0;
		if(hours<=40) {
	salary= wage*hours;
		}
		else if(hours>40)
		{
			salary= (40*wage+(hours-40)*wage*1.5);
		}
		return "Hourly_Employee [wage=" + wage + ", hours=" + hours + ", salary="+salary+"]";
	}
}
