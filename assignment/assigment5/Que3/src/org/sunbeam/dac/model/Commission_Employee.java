/**
 * 
 */
package org.sunbeam.dac.model;

/**
 * @author Admin
 *
 */
public class Commission_Employee extends Employee {

	double grossSales;
	double commissionRate;
	
	public Commission_Employee() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public Commission_Employee(double grossSales, double commissionRate) {
		super();
		this.grossSales = grossSales;
		this.commissionRate = commissionRate;
	}



	public double getGrossSales() {
		return grossSales;
	}



	public void setGrossSales(double grossSales) {
		this.grossSales = grossSales;
	}



	public double getCommissionRate() {
		return commissionRate;
	}



	public void setCommissionRate(double commissionRate) {
		this.commissionRate = commissionRate;
	}



	@Override
	public String toString() {
		
		return super.toString()+"\nCommission_Employee [grossSales=" + grossSales + ", commissionRate=" + commissionRate + "]";
	}



	@Override
	public String showSalary() {
		// TODO Auto-generated method stub
		double salary=0.0;
		salary=commissionRate*grossSales;
		return "Commission_Employee [grossSales=" + grossSales + ", commissionRate=" + commissionRate + ", salary="+salary+"]";
	}
	
	protected double getSalary() {
		return commissionRate*grossSales;
	}

	
}
