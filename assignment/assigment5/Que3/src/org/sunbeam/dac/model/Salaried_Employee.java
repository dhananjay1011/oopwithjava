/**
 * 
 */
package org.sunbeam.dac.model;

/**
 * @author Admin
 *
 */
public class Salaried_Employee extends Employee {

	
	int weeklySalary;
	
	public Salaried_Employee() {
		// TODO Auto-generated constructor stub
	}

	public Salaried_Employee(int weeklySalary) {
		super();
		this.weeklySalary = weeklySalary;
	}

	public int getWeeklySalary() {
		return weeklySalary;
	}

	public void setWeeklySalary(int weeklySalary) {
		this.weeklySalary = weeklySalary;
	}

	@Override
	public String toString() {
		
		return super.toString()+"\nSalaried_Employee [weeklySalary=" + weeklySalary + "]";
	}

	@Override
	public String showSalary() {
		// TODO Auto-generated method stub
		return "Salaried_Employee [weeklySalary=" + weeklySalary + "]";
	}
	
	
}
