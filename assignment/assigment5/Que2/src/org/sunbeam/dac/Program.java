package org.sunbeam.dac;
import java.util.Scanner;

import java.util.Scanner;

class Rectangle {
	private double length;
	private double breadth;

	public Rectangle() {
		this.length = 0;
		this.breadth = 0;
	}

	public Rectangle(double length, double breadth) {
		this.length = length;
		this.breadth = breadth;
	}

	public double calculateArea() {
		return this.length * this.breadth;
	}

	public double calculatePerimeter() {
		double per = 2 * (this.breadth + this.length);
		return per;
	}
}

class Circle {
	private double radius;

	public Circle() {
		this.radius = 0.0;
	}

	public Circle(double radius) {
		this.radius = radius;
	}

	public double calculateArea() {
		double area = Math.PI * Math.pow(this.radius, 2);
		return area;
	}

	public double calculatePerimeter() {
		double per = 2 * Math.PI * this.radius;
		return per;
	}
}

class Triangle {
	private double l1, l2, l3;
	private double s;

	public Triangle(double l1, double l2, double l3) {
		this.l1 = l1;
		this.l2 = l2;
		this.l3 = l3;
		this.s = 0.5 * (this.l1 + this.l2 + this.l3);
	}

	public double calculateArea() {
		double value=this.s*(this.s - this.l1)*(this.s - this.l2)*(this.s - this.l3);
		return Math.sqrt(value);
	}

	public double calculatePerimeter() {
		double per = 2 * this.s;
		return per;
	}
}

public class Program {
	private static Scanner sc = new Scanner(System.in);

	private static int MenuList() {
		System.out.printf("0. Exit\n1. Rectangle\n2. Circle\n3. Triangle\nEnter Choice : ");
		return sc.nextInt();
	}

	public static void main(String[] args) {
		int choice;
		while ((choice = Program.MenuList()) != 0) {
			switch (choice) {
			case 1:
				System.out.print("Enter length : ");
				double length = sc.nextDouble();
				System.out.print("Enter breadth : ");
				double breadth = sc.nextDouble();
				Rectangle rect = new Rectangle(length, breadth);
				System.out.println("Area of rectangle: " + rect.calculateArea());
				System.out.println("Perimeter of rectangle: " + rect.calculatePerimeter());
				break;
			case 2:
				System.out.print("Enter radius : ");
				double radius = sc.nextDouble();
				Circle c = new Circle(radius);
				System.out.println("Area of circle: " + c.calculateArea());
				System.out.println("Perimeter of circle: " + c.calculatePerimeter());
				break;
			case 3:
				System.out.print("Enter l1 : ");
				double l1 = sc.nextDouble();
				System.out.print("Enter l2 : ");
				double l2 = sc.nextDouble();
				System.out.print("Enter l3 : ");
				double l3 = sc.nextDouble();
				Triangle t = new Triangle(l1, l2, l3);
				System.out.println("Area of triangle: " + t.calculateArea());
				System.out.println("Perimeter of triangle: " + t.calculatePerimeter());
				break;
			}
		}
	}
}
