package org.sunbeam.dac;


import java.util.Scanner;

class Date {
	private int day;
	private int month;
	private int year;
	private static Scanner sc = new Scanner(System.in);

	public Date() {
		this.day = 0;
		this.month = 0;
		this.year = 0;
	}

	public void acceptDate() {
		System.out.print("Enter Birth Day : ");
		this.day = sc.nextInt();
		System.out.print("Enter Birth Month : ");
		this.month = sc.nextInt();
		System.out.print("Enter Birth Year : ");
		this.year = sc.nextInt();
	}

	public void printDate() {
		System.out.println(this.day + "/" + this.month + "/" + this.year);
	}
}

class Address {
	private String cityName;
	private String stateName;
	private String pincode;
	private static Scanner sc = new Scanner(System.in);

	public Address() {
		this.cityName = "";
		this.stateName = "";
		this.pincode = "";
	}

	public void acceptAddress() {
		System.out.print("Enter city : ");
		this.cityName = sc.nextLine();
		System.out.print("Enter State : ");
		this.stateName = sc.nextLine();
		System.out.print("Enter pincode : ");
		this.pincode = sc.nextLine();
	}

	public void printAddress() {
		System.out.print("City : " + this.cityName + "  ");
		System.out.print("State : " + this.stateName + "  ");
		System.out.print("Pincode : " + this.pincode);
	}
}

class Person {
	private String name;
	private int age;
	private Date birthDate;
	private Address currentAddress;
	private static Scanner sc = new Scanner(System.in);

	public Person() {
		this.name = "";
		this.age = 0;
		this.birthDate = new Date();
		this.currentAddress = new Address();
	}

	public void acceptRecord() {
		System.out.print("Enter name : ");
		this.name = sc.nextLine();
		System.out.print("Enter age : ");
		this.age = sc.nextInt();
		this.birthDate.acceptDate();
		this.currentAddress.acceptAddress();
	}

	public void printRecord() {
		System.out.println("Name : " + this.name);
		System.out.println("Age : " + this.age);
		System.out.print("Birth Date : ");
		this.birthDate.printDate();
		System.out.print("Current Address : ");
		this.currentAddress.printAddress();
	}
}

public class Program {

	public static void main(String[] args) {
		Person p = new Person();
		p.acceptRecord();
		p.printRecord();
	}

}
