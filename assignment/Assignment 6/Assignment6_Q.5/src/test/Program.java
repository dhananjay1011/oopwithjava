package test;

import java.util.Arrays;
import java.util.Comparator;

abstract class Shape {

	abstract double calarea();

	abstract double calperi();

	@Override
	public String toString() {
		return "Shape [calarea()=" + calarea() + ", calperi()=" + calperi() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}

class Circle extends Shape {
	private double radius;

	public Circle() {
		this(0.0);
	}

	public Circle(double radius) {
		super();
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	@Override
	public String toString() {
		return "circle [radius=" + radius + "]";
	}

	@Override
	double calarea() {
		System.out.println("Circle area");
		return this.radius * this.radius * Math.PI;

	}

	@Override
	double calperi() {
		return 2 * Math.PI * this.radius;

	}

}

class Squre extends Shape {

	private int side;

	public Squre() {
		this(0);
	}

	public Squre(int side) {
		super();
		this.side = side;
	}

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}

	@Override
	public String toString() {
		return "Squre [side=" + side + "]";
	}

	@Override
	double calarea() {
		System.out.println("Squre area");
		return this.side * side;

	}

	@Override
	double calperi() {
		return 4 * this.side;

	}
}

class Triangle extends Shape {
	private double height;
	private double base;

	public Triangle() {
		this(0,0);
	}

	public Triangle(double height, double base) {
		super();
		this.height = height;
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	@Override
	public String toString() {
		return "Triangle [height=" + height + ", base=" + base + "]";
	}

	@Override
	double calarea() {
		System.out.println("Triangle area");
		return 0.5 * this.height * this.base;
	}

	@Override
	double calperi() {
		System.out.println("Triangle per");
		double add = this.height + this.base;

		return 2 * add;
	}

}

class SortByArea implements Comparator<Shape> {
	@Override
	public int compare(Shape s1, Shape s2) {
		if (s1 instanceof Circle && s2 instanceof Circle) {
			Circle cir1 = (Circle) s1;
			Circle cir2 = (Circle) s2;
			return (int) (cir1.calarea() - cir2.calarea());
		} else if (s1 instanceof Squre && s2 instanceof Squre) {
			Squre sq1 = (Squre) s1;
			Squre sq2 = (Squre) s2;
			return (int) (sq1.calarea() - sq2.calarea());
		} else if (s1 instanceof Squre && s2 instanceof Circle) {
			Squre sq1 = (Squre) s1;
			Circle sq2 = (Circle) s2;
			return (int) (sq1.calarea() - sq2.calarea());
		} else if (s1 instanceof Circle && s2 instanceof Squre) {
			Circle sq1 = (Circle) s1;
			Squre sq2 = (Squre) s2;
			return (int) (sq1.calarea() - sq2.calarea());
		}
//		else if( s1 instanceof Triangle && s2 instanceof Triangle )
//		{
//			Triangle t1 = (Triangle) s1;
//			Triangle t2 = (Triangle) s2;
//			return (int) (t1.calarea() - t2.calarea());
//	    }
		return 0;

	}

}

class Program {

	public static Shape[] getShapes() {
		Shape[] arr = new Shape[5];
		arr[0] = new Circle(10);
		arr[1] = new Squre(20);
		arr[2] = new Circle(100);
		arr[3] = new Squre(60);
		arr[4] = new Circle(20);
		return arr;
	}

	private static void print(Shape[] arr) {
		if (arr != null) {
			for (Shape shape : arr)
				System.out.println(shape.calarea());

			System.out.println();
		}
	}

	public static void main(String[] args) {

		Shape[] arr = Program.getShapes();
		Program.print(arr);

		Arrays.sort(arr, new SortByArea());
		Program.print(arr);

	}

}
