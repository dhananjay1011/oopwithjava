package test;

import java.util.Arrays;
import java.util.Comparator;

class Date {
	private int day;
	private int month;
	private int year;

	public Date() {
		this(0, 0, 0);
	}

	public Date(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "Date [day=" + day + ", month=" + month + ", year=" + year + "]";
	}

}

class Person {
	private String name;
	private String address;
	private Date birthDate;

	public Person() {
		this("", "", new Date());
	}

	public Person(String name, String address, Date birthDate) {
		super();
		this.name = name;
		this.address = address;
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", address=" + address + ", birthDate=" + birthDate + "]";
	}

}

class SortByBirthDate implements Comparator<Person> {

	@Override
	public int compare(Person p1, Person p2) {
		return p1.getBirthDate().getYear() - p2.getBirthDate().getYear();
	}

}

public class Program {

	private static Person[] getBirthDate() {
		Person[] arr = new Person[5];
		arr[0] = new Person("Shiavm", "Pune", new Date(2, 9, 1996));
		arr[1] = new Person("Yash", "Bijaypura", new Date(11, 12, 2001));
		arr[2] = new Person("Sangram", "Hyderabad", new Date(22, 5, 1998));
		arr[3] = new Person("Ravindra", "Jaipur", new Date(13, 4, 1992));
		arr[4] = new Person("Mahendra", "Kochi", new Date(7, 7, 1995));
		return arr;
	}

	private static void print(Person[] arr) {
		if (arr != null) {
			for (Person person : arr)
				System.out.println(person.toString());
			System.out.println();
		}

	}

	public static void main(String[] args) {

		Person[] arr = Program.getBirthDate();
		Program.print(arr);

		Comparator<Person> p = null;
		p = new SortByBirthDate();
		Arrays.sort(arr, p);
		Program.print(arr);

	}

}
