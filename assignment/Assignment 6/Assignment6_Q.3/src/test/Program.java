package test;

import java.util.Arrays;
import java.util.Comparator;

class Address {
	private String City;
	private String State;
	private int pincode;

	public Address() {
		this("", "", 0);
	}

	public Address(String city, String state, int pincode) {
		super();
		City = city;
		State = state;
		this.pincode = pincode;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			Address other = (Address) obj;
			if (this.pincode == other.pincode)
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Address [City=" + City + ", State=" + State + ", pincode=" + pincode + "]";
	}

}

class SortByCity implements Comparator<Address> {

	@Override
	public int compare(Address ad1, Address ad2) {
		return ad1.getCity().compareTo(ad2.getCity());
	}

}

public class Program {

	private static Address[] getAddress() {
		Address[] arr = new Address[5];
		arr[0] = new Address("Kolhapur", "Maharashtra", 416007);
		arr[1] = new Address("Bijaypura", "Karnataka", 154230);
		arr[2] = new Address("Hyderabad", "Andhra-Pradesh", 412564);
		arr[3] = new Address("Jaipur", "Rajsthan", 416123);
		arr[4] = new Address("Kochi", "Kerala", 411542);
		return arr;
	}

	private static void print(Address[] arr) {
		if (arr != null) {
			for (Address address : arr)
				System.out.println(address.toString());
			System.out.println();
		}

	}

	public static void main(String[] args) {

		Address[] arr = Program.getAddress();
		Program.print(arr);

		Comparator<Address> a = null;
		a = new SortByCity();
		Arrays.sort(arr, a);
		Program.print(arr);

	}

}
