package test;

import java.util.Arrays;
import java.util.Comparator;

class Date {
	int day;
	int month;
	int year;

	public Date() {
		this(0, 0, 0);
	}

	public Date(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			Date other = (Date) obj;
			if (this.day == other.day)
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Date [day=" + day + ", month=" + month + ", year=" + year + "]";
	}

}

class SortByYear implements Comparator<Date> {

	@Override
	public int compare(Date d1, Date d2) {
		return d1.getYear() - d2.getYear();
	}

}

public class Program {

	private static Date[] getDates() {
		Date[] arr = new Date[5];
		arr[0] = new Date(1, 1, 2020);
		arr[1] = new Date(10, 2, 2018);
		arr[2] = new Date(10, 3, 2017);
		arr[3] = new Date(1, 1, 2010);
		arr[4] = new Date(10, 7, 2010);
		return arr;
	}

	private static void print(Date[] arr) {
		if (arr != null) {
			for (Date date : arr)
				System.out.println(date.toString());
			System.out.println();
		}

	}

	public static void main(String[] args) {

		Date[] arr = Program.getDates();
		Program.print(arr);

		Comparator<Date> d = null;
		d = new SortByYear();
		Arrays.sort(arr, d);
		Program.print(arr);

	
	}

}
