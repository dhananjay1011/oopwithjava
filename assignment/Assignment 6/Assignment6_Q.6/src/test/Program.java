package test;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;

abstract class Player {

	public abstract int income();

}

class CircketPlayer extends Player {
	private int income;

	public CircketPlayer(int income) {
		this.income = income + 20000;
	}

	public int getIncome() {
		return income;
	}

	public void setIncome(int income) {
		this.income = income;
	}

	@Override
	public int income() {

		return this.income;
	}

}

class FootBallPlayer extends Player {
	private int income;

	public FootBallPlayer(int income) {
		this.income = income;
	}

	public int getIncome() {
		return income;
	}

	public void setIncome(int income) {
		this.income = income;
	}

	@Override
	public int income() {

		return this.income + 3000;
	}

}

class sortByIncome implements Comparator<Player> {

	@Override
	public int compare(Player o1, Player o2) {

		if (o1 instanceof FootBallPlayer && o2 instanceof FootBallPlayer) {
			FootBallPlayer f1 = (FootBallPlayer) o1;
			FootBallPlayer f2 = (FootBallPlayer) o2;
			return f1.income() - f2.income();
		} else if (o1 instanceof CircketPlayer && o2 instanceof CircketPlayer) {
			CircketPlayer f1 = (CircketPlayer) o1;
			CircketPlayer f2 = (CircketPlayer) o2;
			return f1.income() - f2.income();
		}

		else if (o1 instanceof CircketPlayer && o2 instanceof FootBallPlayer) {
			CircketPlayer f1 = (CircketPlayer) o1;
			FootBallPlayer f2 = (FootBallPlayer) o2;
			return f1.income() - f2.income();
		}

		else if (o1 instanceof FootBallPlayer && o2 instanceof CircketPlayer) {
			FootBallPlayer f1 = (FootBallPlayer) o1;
			CircketPlayer f2 = (CircketPlayer) o2;
			return f1.income() - f2.income();
		} else {
			return 0;
		}

	}

}

public class Program {

	private static Player[] getPlayers() {
		Player[] arr = new Player[10];
		arr[0] = new CircketPlayer(5000);
		arr[1] = new CircketPlayer(10000);
		arr[2] = new CircketPlayer(10000);
		arr[3] = new CircketPlayer(10000);
		arr[4] = new CircketPlayer(10000);
		arr[5] = new FootBallPlayer(8000);
		arr[6] = new FootBallPlayer(8000);
		arr[7] = new FootBallPlayer(8000);
		arr[8] = new FootBallPlayer(8000);
		arr[9] = new FootBallPlayer(7000);

		return arr;
	}

	private static void printRecord(Player[] arr) {
		if (arr != null) {
			for (Player element : arr) {
				System.out.println(element.income());
			}
		}

	}

	public static void main(String[] args) {
		Player[] arr = getPlayers();

		Arrays.sort(arr, new sortByIncome());
		printRecord(arr);

	}

}