package base;

import java.util.Scanner;

public class Stack {
	private static Scanner sc= new Scanner(System.in);
	private int maxSize,top=-1;
	private int[] stackArray;
	
	public Stack(int i) {
		maxSize=i;
		stackArray=new int[maxSize];
	}
	
	public void push() {
		if(maxSize>top) {
		System.out.println("Enter the number: ");
		stackArray[++top]=sc.nextInt();
//		System.out.println(top);
	}
		else
			System.out.println("Stack is full");
	}
	
	public void pop() {
		if(top>-1)
			stackArray[top--]=0;
		else
			System.out.println("Stack is empty");
			
	}
	
	public void peek() {
		if(top>-1)
		System.out.println(stackArray[top]);
		else
			System.out.println("Stack is empty");

	}
	
	public int menuList() {
		System.out.println("0.EXIT 1.PUSH 2.POP 3.PEEK Enter choice:");
		int i=sc.nextInt();
		return i;
	}
}

