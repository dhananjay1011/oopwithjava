package base;

import java.util.Scanner;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;

public class  SavingsAccount {
	static Scanner sc= new Scanner(System.in);
	private	static float annualInterestRate;
	private double savingsBalance;
	DecimalFormat f = new DecimalFormat("##.##");
	
	public SavingsAccount(double d) {
		savingsBalance=d;
	}

	public double getSavingsBalance() {
		return savingsBalance;
	}



	public void setSavingsBalance(double savingsBalance) {
		this.savingsBalance = savingsBalance;
	}
	
	public void calculateMonthlyInterest() {
		System.out.println();
		double sumInterest=0;
		 String[] months = new DateFormatSymbols().getMonths();
		 for (int i = 0; i <12; i++) {
			sumInterest = sumInterest+(savingsBalance*annualInterestRate/(100*12));
		      System.out.println("month = " + months[i]+ " Interest: "+f.format(sumInterest));
		 }
		 savingsBalance+=sumInterest;
		 System.out.println("Balance: "+f.format(savingsBalance));
		
	}

	public static void modifyInterestRate(float annualInterestRate) {
		SavingsAccount.annualInterestRate=annualInterestRate;
	}




	

	
}