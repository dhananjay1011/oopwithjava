package test;

import base.SavingsAccount;

public class Program {
	public static void main(String[] args) {
		SavingsAccount saver1 = new SavingsAccount(2000.0);
		SavingsAccount saver2= new SavingsAccount(3000.0);
		SavingsAccount.modifyInterestRate(4);
		saver1.calculateMonthlyInterest();
		saver2.calculateMonthlyInterest();
		SavingsAccount.modifyInterestRate(5);
		saver1.calculateMonthlyInterest();
		saver2.calculateMonthlyInterest();
	}
}
