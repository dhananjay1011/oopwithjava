package base;

import java.util.Scanner;

public class Queue {
	static Scanner sc= new Scanner(System.in);
	static final int SIZE=10;
	
	int rear=-1,front=0;
	int[] queue=new int[SIZE];

	
	
	boolean isfull() {
		 if(rear == SIZE - 1)
		      return true;
		   else
		      return false;
		}
	boolean isempty() {
		   if(front < 0 || front > rear) 
		      return true;
		   else
		      return false;
	}
	
	public int menuList() {
		System.out.println("0.EXIT 1.PUSH 2.POP 3.PEEK Enter choice:");
		int i=sc.nextInt();
		return i;
	}


	public void push() {
		sc.nextLine();
		if(isfull())
			System.out.println("Queue is full");
		else {
		System.out.print("Enter element: ");
		queue[++rear]=sc.nextInt();
		}
		
		}
	

	public void pop() {
		sc.nextLine();
		if(isempty())
			System.out.println("Queue is empty");
		else {
			queue[front++]=sc.nextInt();
		}	
	}

	public void peek() {
		sc.nextLine();
		if(isempty())
			System.out.println("Queue is empty");
		else {
			System.out.println(queue[front]);
		}
		
	}
	
}