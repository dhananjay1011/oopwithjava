package test;

import base.Rational;

public class Program {
	public static void main(String[] args) {
		Rational r=new Rational(1,2);
		Rational t=new Rational(4,3);
		Rational n=Rational.add(r,t);
		System.out.println("After addition "+n.show()+" in decimal format "+n.toFloatString(6));
		
		Rational a=Rational.sub(r,t);
		System.out.println("After Substraction "+a.show()+" in decimal format "+a.toFloatString(2));
		
		Rational a1=Rational.mult(r,t);
		System.out.println("After multiplication "+a1.show()+" in decimal format "+a1.toFloatString(3));
		
		Rational a2=Rational.div(r,t);
		System.out.println("After division "+a2.show()+" in decimal format "+a2.toFloatString(4));
		
	}
}
