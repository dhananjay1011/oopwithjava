package base;
/*
Provide a constructor that enables an object of this class to be 
initialized when it�s declared. The constructor should store the 
fraction in reduced form. 
 The fraction 2/4 is equivalent to 1/2 and would be stored in 
the object as 1 in the numerator and 2 in the denominator. 
Provide a no-argument constructor with default values in case no 
initializers are provided. Provide public methods that perform 
each of the following operations: 
a) Add two Rational numbers: The result of the addition should 
be stored in reduced 
form. Implement this as a static method. 
b) Subtract two Rational numbers: The result of the subtraction 
should be stored in reduced form. Implement this as a static 
method. 
c) Multiply two Rational numbers: 
 The result of the multiplication should be stored in reduced 
form. Implement this as a static method. 
d) Divide two Rational numbers: The result of the division 
should be stored in reduced form. 
 Implement this as a static method. 
e) Return a String representation of a Rational number in the 
form a/b, where a is the numerator and b is the denominator. 
f) Return a String representation of a Rational number in 
floating-point format. (Consider providing formatting 
capabilities that enable the user of the class to specify the 
number of digits of precision to the right of the decimal 
point.)
*/
import java.util.Scanner;

import java.text.DecimalFormat;

public class  Rational{
	static Scanner sc= new Scanner(System.in);
	static DecimalFormat f=new DecimalFormat("##.##");
	private int numerator;
	private int denominator;
	
	public Rational(int numerator, int denominator) {
		if(denominator<0) {
		this.numerator = -numerator*1;
		this.denominator=-denominator*1;
			}
		else if(denominator==0) {
			this.denominator=1;
			this.numerator=numerator;
		}
		else {
			this.denominator=denominator;
			this.numerator=numerator;
		}	
		 reduce();
	}
		
	 private void reduce ()
		   {
		      if (numerator != 0)
		      {
		         int common = gcd (Math.abs(numerator), denominator);

		         numerator = numerator / common;
		         denominator = denominator / common;
		      }
		   }

	private int gcd(int num1, int num2) {
		while (num1 != num2)
	         if (num1 > num2)
	            num1 = num1 - num2;
	         else
	            num2 = num2 - num1;

	      return num1;
	}

	public Rational() {
		this.denominator=1;
		this.numerator=1;
	}

	  public String toFloatString( int digits )
	   {
	      double value = ( double ) numerator / denominator;
	      // builds a formatting string that specifies the precision
	      // based on the digits parameter
	      return String.format( "%." + digits + "f", value );
	   } 
		public String show ()
		   {
		      String result;

		      if (numerator == 0)
		         result = "0";
		      else
		         if (denominator == 1)
		            result = numerator + "";
		         else
		            result = numerator + "/" + denominator;
		    
		      return result;
		   }

	public static Rational add(Rational r,Rational t) {
		int num1,dem1,num2,sumnum;
		num1=r.numerator*t.denominator;
		num2=r.denominator*t.numerator;
		dem1=r.denominator*t.denominator;
		sumnum=num1+num2;
		return new Rational(sumnum,dem1);
	}

	public static Rational sub(Rational r,Rational t) {
		int num1,dem1,num2,sumnum;
		num1=r.numerator*t.denominator;
		num2=r.denominator*t.numerator;
		dem1=r.denominator*t.denominator;
		sumnum=num1-num2;
		return new Rational(sumnum,dem1);
	}

	public static Rational mult(Rational r,Rational t) {
		// TODO Auto-generated method stub
		int num1,num2;
		num1=r.numerator*t.numerator;
		num2=r.denominator*t.denominator;
		return new Rational(num1,num2);
	}

	public static Rational div(Rational r,Rational t) {
		int num1,num2;
		num1=r.numerator*t.denominator;
		num2=r.denominator*t.numerator;
		return new Rational(num1,num2);
	}
}