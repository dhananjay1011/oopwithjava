package test;

public enum TrafficLight {

	 RED(60),
	 GREEN(180),
	 YELLOW(50);
	 private final int time;
	 
	 TrafficLight(int r){
	  this.time=r;
	  
	 }
	 public int getTime(){
	  
	  return time;
	 }
	}