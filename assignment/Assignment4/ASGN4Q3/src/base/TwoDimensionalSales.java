package base;

import java.util.Scanner;

public class TwoDimensionalSales{
	static Scanner sc=new Scanner(System.in);
	private int pnum,snum;
	private float amt;
	private double[][] sales=new double[4][5];
	private double totalSalesperson;
	private double[] totalProducts=new double[5];

	
	public int getSnum() {
		return snum;
	}

	public void setSnum(int snum) {
		this.snum = snum;
	}

	public float getAmt() {
		return amt;
	}

	public void setAmt(float amt) {
		this.amt = amt;
	}

//	public double[] getSales() {
//		return sales;
//	}
//
//	public void setSales(double[] sales) {
//		this.sales = sales;
//	}

//	public float getTotal() {
//		return total;
//	}
//
//	public void setTotal(float total) {
//		this.total = total;
//	}

	public double getTotalSalesperson() {
		return totalSalesperson;
	}

	public void setTotalSalesperson(double totalSalesperson) {
		this.totalSalesperson = totalSalesperson;
	}

	public double[] getTotalProducts() {
		return totalProducts;
	}

	public void setTotalProducts(double[] totalProducts) {
		this.totalProducts = totalProducts;
	}
	
	public TwoDimensionalSales(int pnum, int snum, float amt, double[][] sales, float total) {
		super();
		this.setPnum(pnum);
		this.snum = snum;
		this.amt = amt;
		this.sales = sales;
//		this.total = total;
	}

	public TwoDimensionalSales() {
		// TODO Auto-generated constructor stub
	}

	public int getPnum() {
		return pnum;
	}

	public void setPnum(int pnum) {
		this.pnum = pnum;
	}
	public void sales(){
		int ch=1;
		while(ch==1) {
			System.out.print("Enter salesperson number: ");
			setSnum(sc.nextInt());
			System.out.print("Enter product number: ");
			setPnum(sc.nextInt());
			if(snum>0 && snum<5 && pnum>0 && pnum<6) {
				System.out.print("Enter amount: ");
				setAmt(sc.nextFloat());
				sales[snum-1][pnum-1]+=amt;
			}
			else
				System.out.println("Invalid Input");
			
			
		System.out.println("Enter 0.Exit 1.Enter details");
		ch=sc.nextInt();
		
	}
	}

	public void print() {
		System.out.println("salesperson| product1 | product2 | product3 | product4 | product5  ");
		for(int i=0; i<4; i++) {
			System.out.print("\nsalesperson"+(i+1));
			for (int j=0; j<5; j++) {
				System.out.print("\t"+sales[i][j]);
				totalSalesperson+=sales[i][j];
				totalProducts[j]+=sales[i][j];
			}
			System.out.print("\t"+totalSalesperson);
			totalSalesperson=0;
		}
		System.out.print("\nTotal : ");
		for (int j=0; j<5; j++)
			System.out.print("\t"+totalProducts[j]);
	}

	
	
}