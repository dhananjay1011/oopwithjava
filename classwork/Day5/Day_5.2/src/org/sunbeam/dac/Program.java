package org.sunbeam.dac;

class Date{
	private int day;
	private int month;
	private int year;
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
}
class Employee{
	private int empid;
	private Date joinDate;
	public Employee(int empid, int day, int month, int year ) {
		this.empid = empid;
		this.joinDate = new Date(day, month, year);
	}
}
public class Program {
	public static void main(String[] args) {
		Employee emp = new Employee(33, 26,12,2006);
		//Employee emp => Reference =>  Java Stack
		//new Employee(33, 26,12,2006); => Instance => Heap
	}
	public static void main1(String[] args) {
		Date joinDate = new Date( 26,12,2006);
		//Date joinDate => Reference =>  Java Stack
		//new Date( 26,12,2006); => Instance => Heap
	}
}
