package org.sunbeam.dac;
class Test{
	private int num1;	//Instance variable
	private int num2;	//Instance variable
	private static int num3 ;	//Class Level variable
	static {	//Static Initialization Block
		num3 = 0;
	}
	public Test() {
		this.num1 = 0;
		this.num2 = 0;
	}
	
	public void setNum1(int num1) {
		this.num1 = num1;
	}

	public void setNum2(int num2) {
		this.num2 = num2;
	}

	public static void setNum3(int num3) {
		Test.num3 = num3;
	}

	public void printRecord( ) {
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+this.num2);
		System.out.println("Num3	:	"+Test.num3 );
	}
}
public class Program {	
	public static void main(String[] args) {
		
		Test t1 = new Test( );
		t1.setNum1(10);
		t1.setNum2(20);
		t1.printRecord();
		Test.setNum3(500);
		
		
		Test t2 = new Test( );
		t2.setNum1(30);
		t2.setNum2(40);
		t2.printRecord();
		
	}
}

