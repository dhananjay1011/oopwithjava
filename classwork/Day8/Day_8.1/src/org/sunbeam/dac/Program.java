package org.sunbeam.dac;
class Test{
	public static void showRecord( ) {
		System.out.println("Test.showRecord()");
	}
}
public class Program {
	public static void displayRecord( ) {
		System.out.println("Program.displayRecord()");
	}
	public static void main(String[] args) {
		Program.displayRecord();	//OK
		displayRecord();			//OK
		
		Test.showRecord();			//OK
		showRecord();			//NOT OK
	}
}
