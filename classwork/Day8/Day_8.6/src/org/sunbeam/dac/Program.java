package org.sunbeam.dac;

import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	private static void acceptRecord(int[][] arr) {
		if( arr != null ) {
			for( int row = 0; row < 4; ++ row ) {
				for( int col = 0; col < 3; ++ col ) {
					System.out.print("Enter element	:	");
					arr[ row ][ col ] = sc.nextInt();
				}	 
			}
		}
	}
	private static void printRecord(int[][] arr) {
		if( arr != null ) {
			for( int row = 0; row < 4; ++ row ) {
				for( int col = 0; col < 3; ++ col ) {
					System.out.print(arr[ row ][ col ]+"	");
				}
				System.out.println();
			}
		}
	}
	public static void main(String[] args) {
		int[][] arr = new int[ 4 ][ 3 ];
		Program.acceptRecord( arr );
		Program.printRecord( arr );
	}
	public static void main4(String[] args) {
		int[][] arr = new int[ 4 ][ 3 ];
		for( int row = 0; row < 4; ++ row ) {
			for( int col = 0; col < 3; ++ col ) {
				System.out.print(arr[ row ][ col ]+"	");
			}
			System.out.println();
		}
	}
	public static void main3(String[] args) {
		int[][] arr = new int[ 4 ][ 3 ];
	}
	public static void main2(String[] args) {
		int[][] arr = null;
		arr = new int[ 4 ][ 3 ];
	}
	public static void main1(String[] args) {
		//int arr[ ][ ];	//OK
		//int[ ] arr[ ];	//OK
		//int[][] arr;	//OK
	}
}
