package org.sunbeam.dac;
import static java.lang.System.out;
import static java.lang.Math.*;
public class Program {
	public static void main(String[ ] args) {
		double radius = 10.5d;
		double area = PI * pow(radius, 2);
		out.println("Area	:	"+area);
	}
	public static void main2(String[] args) {
		double radius = 10.5d;
		double area = Math.PI * Math.pow(radius, 2);	//PI and pow are static members of java.lang.Math class
		System.out.println("Area	:	"+area);
	}
	public static void main1(String[] args) {
		double radius = 10.5d;
		double area = 3.142 * radius * radius;
		System.out.println("Area	:	"+area);
	}
}
