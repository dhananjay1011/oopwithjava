package org.sunbeam.dac;

import java.util.Arrays;

public class Program {
	public static void main(String[] args) {
		int[] arr = new int[  ] { 10, 20, 30 };
		for(  int element : arr ) {
			System.out.println(element);
		}
	}
	public static void main5(String[] args) {
		int[] arr = new int[  ] { 50, 10, 40, 20, 30 };	
		System.out.println(Arrays.toString(arr));
		System.out.println();
		Arrays.sort(arr);	// Dual-Pivot Quicksort
		System.out.println(Arrays.toString(arr));
	}
	public static void main4(String[] args) {
		int[] arr = new int[  ] { 10, 20, 30 };	//OK
		//int element = arr[ -1 ];	//ArrayIndexOutOfBoundsException
		int element = arr[ arr.length ];	//ArrayIndexOutOfBoundsException
		System.out.println("Element	:	"+element);	//10
	}
	public static void main3(String[] args) {
		//int[] arr = new int[ 3 ];	//OK
		//int[] arr = new int[ 3 ] { 10, 20, 30 };	//Not OK
		//int[] arr = new int[  ] { 10, 20, 30 };	//OK
		int[] arr =  { 10, 20, 30 };	//OK
		System.out.println(Arrays.toString(arr));
	}
	public static void main2(String[] args) {
		int[] arr = new int[ 3 ];
		System.out.println(Arrays.toString(arr));
		
		//String str = Arrays.toString(arr);
		//System.out.println(str);	//[0, 0, 0]
	}
	public static void main1(String[] args) {
		int[] arr = new int[ 3 ];
		for( int index = 0; index < arr.length; ++ index )
			System.out.println(arr[ index ] ) ;
	}
}
