package org.sunbeam.dac;

import java.util.Scanner;

class Employee{
	String name;	//Field
	int empid;		//Field
	float salary;	//Field
}
public class Program {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		Employee emp = new Employee();
		System.out.print("Name	:	");
		emp.name = sc.nextLine();
		
		System.out.print("Empid	:	");
		emp.empid = sc.nextInt();
		
		System.out.print("Salary	:	");
		emp.salary = sc.nextFloat();
		
		System.out.println(emp.name+"	"+emp.empid+"	"+emp.salary);
		
	}
	public static void main1(String[] args) {
		//Employee emp;	//object reference / reference
		//new Employee();	//Anonymous Instance
		
		Employee emp1;
		emp1 = new Employee();	//OK
		
		Employee emp2 = new Employee();	//OK
	}
}
