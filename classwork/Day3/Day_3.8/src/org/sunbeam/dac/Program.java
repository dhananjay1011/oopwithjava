package org.sunbeam.dac;

import java.util.Scanner;

class Employee{
	//Instance variable
	private String name;	//Field
	private int empid;		//Field
	private float salary;	//Field
	
	//Concrete Method => Instance Method
	public void acceptRecord( ) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Name	:	");
		name = sc.nextLine();
		System.out.print("Empid	:	");
		empid = sc.nextInt();
		System.out.print("Salary	:	");
		salary = sc.nextFloat();
	}
	
	//Concrete Method => Instance Method
	public void printRecord( ) {
		System.out.println("Name	:	"+name);
		System.out.println("Empid	:	"+empid);
		System.out.println("Salary	:	"+salary);
	}
}
public class Program {
	public static void main1(String[] args) {
		Employee e1 = new Employee();
		Employee e2 = e1;
		
		Employee e3 = new Employee();
	}
	public static void main2(String[] args) {
		Employee emp = new Employee();	//Instantiation
		emp.acceptRecord( );	//Message passing
		emp.printRecord( );		//Message passing
	}
	public static void main(String[] args) {
		Integer n1 = new Integer(125);
	}
}
