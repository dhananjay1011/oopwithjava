package org.sunbeam.dac.launcher;

enum Day{
	SUN("SunDay"), MON(2), TUES("TuesDay", 3);	//Must be first line
	private String dayName;
	private int dayNumber;
	private Day(String dayName) {
		this.dayName = dayName;
	}
	private Day(int dayNumber) {
		this.dayNumber = dayNumber;
	}
	private Day(String dayName, int dayNumber) {
		this.dayName = dayName;
		this.dayNumber = dayNumber;
	}
	public String getDayName() {
		return dayName;
	}
	public int getDayNumber() {
		return dayNumber;
	}
}
public class Program {
	public static void main(String[] args) {
		Day[] days = Day.values();
		for (Day day : days) {
			System.out.printf("%-15s%-5d%-15s%-5d\n", day.name(), day.ordinal(), day.getDayName(), day.getDayNumber());
		}
	}
}
