package org.sunbeam.dac.launcher;

import java.util.Scanner;

enum Color{
	RED, GREEN, BLUE
}
public class Program {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Color Name	");
		String colorName = sc.nextLine();
		
		Color color = Color.valueOf(colorName.toUpperCase());
		System.out.printf("%-10s%-5d\n",color.name(), color.ordinal());
	}
	public static void main6(String[] args) {
		int number = 125;
		String strNumber = String.valueOf(number);
		System.out.println("Number	:	"+strNumber);
	}
	public static void main5(String[] args) {
		Color[] colors = Color.values();
		for (Color color : colors) {
			System.out.printf("%-10s%-5d\n",color.name(), color.ordinal());
		}
	}
	public static void main4(String[] args) {
		Color color = Color.BLUE;
		String name = color.name();
		int ordinal = color.ordinal();
		
		System.out.println("Name	:	"+name);
		System.out.println("Ordinal	:	"+ordinal);
	}
	public static void main3(String[] args) {
		Color color = Color.GREEN;
		String name = color.name();
		int ordinal = color.ordinal();
		
		System.out.println("Name	:	"+name);
		System.out.println("Ordinal	:	"+ordinal);
	}
	public static void main2(String[] args) {
		Color color = Color.RED;
		String name = color.name();
		int ordinal = color.ordinal();
		
		System.out.println("Name	:	"+name);
		System.out.println("Ordinal	:	"+ordinal);
	}
	public static void main1(String[] args) {
		String name = Color.RED.name();
		int ordinal = Color.RED.ordinal();
	}
}
