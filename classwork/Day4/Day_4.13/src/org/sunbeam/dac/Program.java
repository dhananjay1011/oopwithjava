package org.sunbeam.dac;

class Complex{
	private int real;
	private int imag;
	//Default constructor : Compiler generated zero argument constructor
	/*
	 	public Complex( ){
	 		//TODO: Empty
	 	}
	 */
	public void printRecord() {
		System.out.println(this.real+"	"+this.imag);
	}
}
public class Program {
	public static void main(String[] args) {
		Complex c1 = new Complex();	//OK
		
		Complex c2 = new Complex(50,60);	//NOT OK
	}
}














