package org.sunbeam.dac;

import java.util.Scanner;
public class Program {
	public static void main(String[] args) {
		class Date {
			int day;	//Field
			int month;	//Field
			int year;	//Field
			
			void acceptRecord( /*Date this*/ ) {
				Scanner sc = new Scanner(System.in);
				System.out.print("Day	:	");
				this.day = sc.nextInt();
				System.out.print("Month	:	");
				this.month = sc.nextInt();
				System.out.print("Year	:	");
				this.year = sc.nextInt();
			}
			void printRecord( /*Date this */) {
				System.out.println(this.day+" / "+this.month+" / "+this.year);
			}
		} 
		
		Date dt1 = new Date( );	//OK : Instantiation
		dt1.acceptRecord( );	//dt1.acceptRecord( dt1 );
		dt1.printRecord( );	//dt1.printRecord( dt1 );
		
		/*Date dt2 = new Date( );	//OK : Instantiation
		dt2.acceptRecord();	//dt2.acceptRecord(dt2);
		dt2.printRecord();//dt2.printRecord(dt2);*/
		
	}
}
