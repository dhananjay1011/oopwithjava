package org.sunbeam.dac;

class Complex{
	private int real;
	private int imag;

	public Complex( ) {
		System.out.println("public Complex( )");
		this.real = 10;
		this.imag = 20;
	}

	public Complex( int real, int imag ) {
		System.out.println("public Complex( int real, int imag )");
		this.real = real;
		this.imag = imag;
	}
	public void printRecord() {
		System.out.println(this.real+"	"+this.imag);
	}
}
public class Program {
	public static void main(String[] args) {
		Complex c1 = new Complex( 50, 60 );
		
		Complex c2 = new Complex( );
	}
}














