package org.sunbeam.dac;

import java.util.Scanner;

/*Access Modifiers In Java:
	1. private( - )
	2. package level private( ~ )
	3. protected( # )
	4. public( + )
*/

public class Program {
	public static void main(String[] args) {
		
		//int num1 = 10;	//OK
		//int num2 = new int(20);	//Not OK
		
		
		Scanner sc = new Scanner(System.in);

		class Date {
			int day;	//Field
			int month;	//Field
			int year;	//Field
		}
		//C : struct Date dt;	//dt is object of struct Date
		//Java	:  Date dt;	//dt is object reference/reference	
		//Java	:	new Date( );	//new Date( ) -> Instance
		
		//Date dt;	//Method Local Variable => Java Stack
		//dt = new Date( );	//Java Instance => Heap
		
		//ClassName refName = new ClassName( ); <=  Instantiation 
		Date dt = new Date( );	//OK : Instantiation

		System.out.print("Day	:	");
		dt.day = sc.nextInt();
		System.out.print("Month	:	");
		dt.month = sc.nextInt();
		System.out.print("year	:	");
		dt.year = sc.nextInt();

		System.out.println(dt.day + " / " + dt.month + " / " + dt.year);
	}
}
