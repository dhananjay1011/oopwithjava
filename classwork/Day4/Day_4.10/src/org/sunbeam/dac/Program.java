package org.sunbeam.dac;

class Complex{
	private int real;
	private int imag;
	//Parameterless constructor / zero argument constructor 
	//User Defined Default Construcpt
	public Complex( ) {
		this.real = 10;
		this.imag = 20;
	}
	public void printRecord() {
		System.out.println(this.real+"	"+this.imag);
	}
}
public class Program {
	public static void main(String[] args) {
		Complex c1 = new Complex( );
		c1.printRecord();
	}
}














