package org.sunbeam.dac;

class Complex{
	private int real;
	private int imag;
	//Parameterized constructor
	public Complex( int value ) {
		this.real = value;
		this.imag = value;
	}
	//Parameterized constructor
	public Complex( int real, int imag ) {
		this.real = real;
		this.imag = imag;
	}
	public void printRecord() {
		System.out.println(this.real+"	"+this.imag);
	}
}
public class Program {
	public static void main(String[] args) {
		Complex c1 = new Complex( 50, 60 );
		c1.printRecord();
		
		Complex c2 = new Complex( 70 );
	}
}














