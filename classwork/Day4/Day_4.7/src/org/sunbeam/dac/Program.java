package org.sunbeam.dac;

import java.util.Scanner;

class Employee{
	private String name;	//null
	private int empid;		//0
	private float salary;	//0.0
	public void printRecord( ) {
		System.out.println(this.name+"	"+this.empid+"	"+this.salary);
	}
}
public class Program {
	public static void main(String[] args) {
		Employee emp = null;
		emp = new Employee();
		emp.printRecord();	
	}
	public static void main3(String[] args) {
		Employee emp = null;
		//Here emp is called null reference variable / null object.
		emp.printRecord();	//NullPointerException
	}
	public static void main2(String[] args) {
		Employee emp = new Employee();
		emp.printRecord();
	}
	public static void main1(String[] args) {
		Employee emp;
		emp = new Employee();
		emp.printRecord();
	}
}

















