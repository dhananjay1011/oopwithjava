class Program{
    public static void main3(String[] args) {
        int num1 = 10;  //OK
        int num2;   //OK
        num2 = num1;    //OK
        System.out.println("Num1  :   "+num1); //10
        System.out.println("Num2  :   "+num2); //20
    }
    public static void main2( String[] args ){
        int number;
        number = 10;
        System.out.println("Number  :   "+number);
    }
    public static void main1( String[] args ){
        int number = 10;
        //System.out.println(number); //10
        System.out.println("Number  :   "+number); //Number : 10
    }
}
