class Program{
    public static void main(String[] args) {
        String s1 = "10";
        int num1 = Integer.parseInt(s1);    //UnBoxing
        System.out.println("Num1    :   "+num1);

        String s2 = "10.2f";
        float num2 = Float.parseFloat(s2);  //UnBoxing
        System.out.println("Num2    :   "+num2);

        String s3 = "10.5d";
        double num3 = Double.parseDouble(s3);   //UnBoxing
        System.out.println("Num3    :   "+num3);

    }
    public static void main3(String[] args) {
        double number = 10.5d;                          
        //String str = Double.toString(number); //Boxing
        String str = String.valueOf(number);    //Boxing
        System.out.println( str );
    }
    public static void main2(String[] args) {
        float number = 10.5f;                          

        //String str = Float.toString(number);    //Boxing
        String str = String.valueOf( number );     //Boxing

        System.out.println( str );
    }
    public static void main1(String[] args) {
        //int   => Primitive / Value Type
        int number = 10;                          

        //String => Non Primitive / Reference Type 
        //String str = Integer.toString( number );    //Boxing    
        String str = String.valueOf(  number );    //Boxing 
        System.out.println( str );
    }
}