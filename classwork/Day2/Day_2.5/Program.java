class Program{
    public static void main(String[] args) {
        double num1 = 10.5;  //Initialization
        int num2 = ( int )num1; //Narrowing : OK
        int num3 = num1; //Narrowing : NOT OK

        System.out.println("Num1  :   "+num1);
        System.out.println("Num2  :   "+num2);
        System.out.println("Num3  :   "+num3);
    }
    public static void main3(String[] args) {
        int num1 = 10;  //Initialization
        double num2 = (double)num1;  //Widening : OK
        double num3 = num1;  //Widening : OK

        System.out.println("Num1  :   "+num1);
        System.out.println("Num2  :   "+num2);
        System.out.println("Num3  :   "+num3);
    }
    public static void main2(String[] args) {
        double num1 = 10.5;  //Initialization
        double num2 = num1;  //Initialization
        System.out.println("Num1  :   "+num1);
        System.out.println("Num2  :   "+num2);
    }
    public static void main1(String[] args) {
        int num1 = 10;  //Initialization
        int num2 = num1;  //Initialization
        System.out.println("Num1  :   "+num1);
        System.out.println("Num2  :   "+num2);
    }
}