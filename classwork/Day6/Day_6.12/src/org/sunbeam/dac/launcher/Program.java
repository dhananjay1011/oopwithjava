package org.sunbeam.dac.launcher;

import java.util.Scanner;

import org.sunbeam.dac.model.Complex;

public class Program {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		Complex complex = new Complex();
		
		System.out.print("Real Number	:	");
		complex.setReal( sc.nextInt() );
		System.out.print("Imag Number	:	");
		complex.setImag(sc.nextInt());
		
		System.out.println("Real Number	:	"+complex.getReal());
		System.out.println("Imag Number	:	"+complex.getImag());
		
	}
}
