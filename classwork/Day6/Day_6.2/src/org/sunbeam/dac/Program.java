package org.sunbeam.dac;

public class Program {
	private static int count;
	public static void print( ) {
		count = count + 1;
		System.out.println("Count	:	"+count);
	}
	public static void main(String[] args) {
		Program.print();	//1
		Program.print();	//2
		Program.print();	//3
	}
}
