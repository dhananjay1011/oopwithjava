package org.sunbeam.dac;

class Test{
	public static void showRecord( ) {
		System.out.println("Inside showRecord");
	}
	public void displyRecord( ) {
		System.out.println("Inside showRecord");
	}
}
public class Program {
	public static void main(String[] args) {
		Test t = new Test();
		t.displyRecord();
		t.showRecord();	//OK : The static method showRecord() from the type Test should be accessed in a static way
	}
	public static void main1(String[] args) {
		Test.showRecord();	//OK
		//Test.displyRecord( );	//Not OK
	}
}



