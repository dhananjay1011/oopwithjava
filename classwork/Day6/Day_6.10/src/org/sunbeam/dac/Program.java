package org.sunbeam.dac;

class Test{
	public static final int number = 10;
	public void showRecord( ) {
		//this.number  = this.number + 1;	//Not OK
		System.out.println("Number	:	"+Test.number);
	}
	public void displayRecord( ) {
		//this.number  = this.number + 1;	//Not OK
		System.out.println("Number	:	"+Test.number);
	}
}
public class Program {
	public static void main(String[] args) {
		Test t = new Test( );
		t.showRecord();
		t.displayRecord();
	}
}