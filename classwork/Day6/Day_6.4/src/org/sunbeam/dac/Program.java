package org.sunbeam.dac;
//What is singleton class : A class from which we can create only once instance is called
//singleton class.

class Singleton{
	private int number;
	private Singleton() {
		this.number = 10;
	}
	public int getNumber() {
		return number;
	}
	private static Singleton instance;	//null
	public static Singleton getInstance( ) {
		if( instance == null ) 
			instance = new Singleton();
		return instance;
	}
}
public class Program {
	public static void main(String[] args) {
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		System.out.println(s1 == s2 );
	}
	public static void main1(String[] args) {
		Singleton s1 = Singleton.getInstance();
		System.out.println("Number	:	"+s1.getNumber());
	}
}
