package org.sunbeam.dac;
class Singleton{
	private static Singleton instance; 
	static {
		instance  = new Singleton();
	}
	private int number;
	private Singleton( ){
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public static Singleton getInstance() {
		return instance;
	}
}
public class Program {
	public static void main(String[] args) {
		Singleton s1 = Singleton.getInstance();
		s1.setNumber(50);
		System.out.println("Number	:	"+s1.getNumber());
	}
}
